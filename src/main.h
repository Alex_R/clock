#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <ncurses.h>


/* Type definitions. */
struct _window {
    WINDOW *window;
    int x, y;
};


/* Function prototypes. */
int main(int argc, char *argv[]);

#endif
