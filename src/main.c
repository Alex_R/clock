#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ncurses.h>
#include <time.h>
#include <limits.h>

#include "main.h"


static struct _window _window;


static void _die(const char *message) {
    fprintf(stderr, "%s\n", message);
    abort();
}

static void _init_curses(void) {
    _window.window = initscr();
    if(_window.window == NULL) {
        _die("initscr() failed");
    }

    if(raw() == ERR) {
        _die("raw() failed");
    }

    if(noecho() == ERR) {
        _die("noecho() failed");
    }

    if(curs_set(0) == ERR) {
        _die("curs_set() failed");
    }

    timeout(1000);
}

static void _end_curses(void) {
    if(endwin() == ERR) {
        _die("endwin() failed");
    }

    if(delwin(_window.window) == ERR) {
        _die("delwin() failed");
    }
}

static void _update_size(void) {
    getmaxyx(_window.window, _window.y, _window.x);
    if(_window.x == -1 || _window.y == -1) {
        _die("getmaxyx() failed");
    }
}

static void _move_cursor(int x, int y) {
    if(move(y, x) == ERR) {
        fprintf(stderr, "move() failed");
    }
}


static void _get_time(char time_str[NAME_MAX], char date_str[NAME_MAX]) {
    time_t raw_time;
    struct tm local_time;

    raw_time = time(NULL);
    if(raw_time == -1) {
        _die("time() failed");
    }

    if(localtime_r(&raw_time, &local_time) == NULL) {
        _die("localtime_r() failed");
    }

    if(strftime(time_str, NAME_MAX, "%T", &local_time) == 0) {
        _die("strftime() failed");
    }

    if(strftime(date_str, NAME_MAX, "%A, %B %d, %Y", &local_time) == 0) {
        _die("strftime() failed");
    }
}


static void _draw_time(char *time_str, char *date_str) {
    if(erase() == ERR) {
        _die("erase() failed");
    }

    _move_cursor(((_window.x / 2) - (strlen(time_str) / 2)), ((_window.y / 2) - 1));
    if(addstr(time_str) == ERR) {
        fprintf(stderr, "addstr() failed");
    }

    _move_cursor(((_window.x / 2) - (strlen(date_str) / 2)), ((_window.y / 2) + 1));
    if(addstr(date_str) == ERR) {
        fprintf(stderr, "addstr() failed");
    }
}


int main(int argc, char *argv[]) {
    char time_str[NAME_MAX], date_str[NAME_MAX];
    int ch;

    (void) argc;
    (void) argv;

    _init_curses();
    if(atexit(_end_curses) != 0) {
        _die("atexit() failed");
    }

    while(1) {
        _get_time(time_str, date_str);
        _update_size();
        _draw_time(time_str, date_str);

        ch = getch();
        if(ch != ERR) {
            if(ch == 'q') {
                return EXIT_SUCCESS;
            }
        }
    }

    return EXIT_SUCCESS;
}
